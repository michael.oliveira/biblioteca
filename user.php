<?php 
    include "layout.php";
    require "ClienteDao.php";
    $clienteDao = ClienteDao::getInstance();
    $id = (int) isset($_GET['id']) ? $_GET['id'] : 0;
    $clienteModel = $clienteDao->getCliente($id);

?>
<form method="post" action="recebe.php?op=add_user">
<div class="container">
<div id="accordion">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <h3>Cadastro de Usuário</h3>
            </div>
        </div>
    </div>

    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <i class="glyphicon glyphicon-user text-gold"></i>
                    <b>Dados de Usuario</b>
                </a>
            </h4>
        </div>
        <div id="collapse1" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $clienteModel->getNome(); ?></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Nome de Usuario</label>
                            <input name="user_name" type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Senha</label>
                            <input name="senha" type="password" class="form-control" />
                        </div>
                    </div>
                </div>

 
            </div>
        </div>
    </div>
    
    <br />
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <input type="submit" value="Salvar" 
                   class="btn btn-success btn-lg" id="btnSubmit">
            <a class="btn btn-warning btn-lg" 
               href="index.php" id="btnToTop">
                <i class="fa fa-arrow-left"></i> Voltar</a>
        </div>
    </div>
</div>
</div>
</form>

<script type="text/javascript">
    $('#dt_nasc').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-br'
    });
</script>
    