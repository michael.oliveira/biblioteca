<?php
extract($_POST);

    include_once "layout.php";
    require "ClienteDao.php";
    require_once "ClienteModel.php";
    require_once "PessoaModel.php";
    
    $clienteDao = ClienteDao::getInstance();
    
    $op = (isset($_GET['op']) ? $_GET['op'] : "");
    $id = (isset($_GET['id']) ? $_GET['id'] : 0);
    switch($op){
        case 'add_user':
            try{
                $msg = "Usuario cadastrado com sucesso com sucesso! ";
                $class = "alert alert-success";
                $clienteDao->insertUser($id, $_POST);
            } catch (PDOException $ex) {

            }
        break;
        case 'delete':
            try{
                    $clienteDao->delete($id);
                    $msg = "Cliente deletado com sucesso! ";
                    $class = "alert alert-success";
                        
                } catch (PDOException $e) {
                    $msg = "Erro ao tentar excluir o cliente ";
                    $class = "alert alert-danger";
                    echo "Error: " . $e->getMessage();
                }
                
                echo "<div class='container'>"
                    . "<div class='" . $class . "'>" . $msg . "</div>"
                        . "<a href='index.php' class='btn btn-primary'>Voltar</a>"
                    . "</div>";
                
            break;
        case 'adicionar':
            try{
                $sql = "INSERT INTO cliente(nome, rg, cpf, endereco, bairro, cidade, uf) "
                    . "VALUES(:nome, :rg, :cpf, :endereco, :bairro, :cidade, :uf)";
                $msg = "<div class='container'>"
                    . "<br><div class='alert alert-success fade in'>"
                    . "<a href='#' class='close' data-dismiss='alert'"
                    . "aria-label='close'>&times;</a>"
                    . "<strong>Cadastro realizado com Sucesso!</strong>"
                    . "</div><br>";
                $msg .= "<br><a href=\"./index.php\" "
                    . "class=\"btn btn-primary\">Voltar</a></div>";
            
                    if (!empty($_POST)){
                        $pessoaModel = new PessoaModel();
                        $clienteModel = new ClienteModel();
                        $pessoaModel->setNome($nome);
                        $pessoaModel->setRg($rg);
                        $pessoaModel->setCpf($cpf);
                        $pessoaModel->setData_nascimento($clienteDao->dataEua($data_nascimento));
                        $pessoaModel->setCep($cep);
                        $pessoaModel->setEndereco($endereco);
                        $pessoaModel->setNumero($numero);
                        $pessoaModel->setBairro($bairro);
                        $pessoaModel->setCidade($cidade);
                        $pessoaModel->setEstado($estado);
                        $clienteModel->setPessoa($pessoaModel);
                        
                        $clienteDao->insert($clienteModel);
                        
                        echo $msg;
                    }
                } catch(PDOException $e){
                    echo $e->getMessage();
                }
                break;
        case 'editar' :
            $sql = "UPDATE cliente SET nome = :nome, rg = :rg";
            $msg = "Cadastro atualizado com Sucesso!";
            $msg .= "<br><a href=\"/index.php?id=$id\">Voltar</a>";
            
    }