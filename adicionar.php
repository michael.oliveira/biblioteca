<?php 
    include "layout.php";
?>
<form method="post" action="recebe.php?op=adicionar">
<div class="container">
<div id="accordion">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <h3>Cadastro de Cliente</h3>
            </div>
        </div>
    </div>

    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <i class="glyphicon glyphicon-search text-gold"></i>
                    <b>Dados Pessoais</b>
                </a>
            </h4>
        </div>
        <div id="collapse1" class="collapse show">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Nome</label>
                            <input name="nome" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">CPF</label>
                            <input name="cpf" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">RG</label>
                            <input name="rg" class="form-control" type="text" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Data de Nascimento</label>
                            <div class="input-group date">
                                <input name="data_nascimento" 
                                       id="dt_nasc" class="form-control" type="text" />
                               <span class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">CEP</label>
                            <input name="cep" type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Endereço</label>
                            <input name="endereco" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label">Numero</label>
                            <input name="numero" type="text" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Bairro</label>
                            <input name="bairro" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Cidade</label>
                            <input name="cidade" type="text" class="form-control" />
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select name="estado" class="form-control">
                                <option value="AP" selected="true">Amapá</option>
                                <option value="PA">Pará</option>
                                <option value="AM">Amazonas</option>
                            </select>
                        </div>
                    </div>

                </div>
 
            </div>
        </div>
    </div>
    
    <br />
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <input type="submit" value="Salvar" 
                   class="btn btn-success btn-lg" id="btnSubmit">
            <a class="btn btn-warning btn-lg" 
               href="index.php" id="btnToTop">
                <i class="fa fa-arrow-left"></i> Voltar</a>
        </div>
    </div>
</div>
</div>
</form>

<script type="text/javascript">
    $('#dt_nasc').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-br'
    });
</script>
    