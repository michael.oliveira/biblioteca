<?php 
    include "layout.php";
    require_once "ClienteDao.php";
    
    $clienteDao = ClienteDao::getInstance();
    $id = isset($_GET['id']) ? $_GET['id'] : 0; 
    
    $cliente = $clienteDao->getCliente($id);
?>
<form method="post" action="recebe.php?op=editar&id=<?php echo $id ?>">
    <div class="container">
        <div id="accordion">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <h3>Editar Cliente</h3>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            <i class="glyphicon glyphicon-search text-gold"></i>
                            <b>Dados Pessoais</b>
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="collapse show">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Nome</label>
                                    <input name="nome" 
                                    value='<?php echo $cliente->getPessoa()->getNome() ?>'
                                    type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">CPF</label>
                                    <input name="cpf" 
                                    value='<?php echo $cliente->getPessoa()->getCpf() ?>'        
                                    type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">RG</label>
                                    <input name="rg" 
                                        value='<?php echo $cliente->getPessoa()->getRg() ?>'    
                                        class="form-control" type="text" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Data de Nascimento</label>
                                    <div class="input-group date">
                                        <input name="data_nascimento" 
                                        value='<?php echo $clienteDao->dataBr(
                                                $cliente->getPessoa()->getData_nascimento()) ?>'
                                        id="dt_nasc" class="form-control" type="text" />
                                       <span class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button">
                                                <i class="glyphicon glyphicon-calendar"></i>
                                            </button></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">CEP</label>
                                    <input name="cep" 
                                    value='<?php echo $cliente->getPessoa()->getCep() ?>' 
                                    type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Endereço</label>
                                    <input name="endereco" 
                                    value='<?php echo $cliente->getPessoa()->getEndereco() ?>' 
                                    type="text" class="form-control" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Bairro</label>
                                    <input name="bairro" 
                                    value='<?php echo $cliente->getPessoa()->getBairro() ?>'        
                                    type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Cidade</label>
                                    <input name="cidade" 
                                    value='<?php echo $cliente->getPessoa()->getCidade() ?>'        
                                    type="text" class="form-control" />
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Estado</label>
                                    <select name="estado" class="form-control">
                                        <option value="AP" selected="true">Amapá</option>
                                        <option value="PA">Pará</option>
                                        <option value="AM">Amazonas</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <br />
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <input type="submit" value="Salvar" 
                           class="btn btn-success btn-lg" id="btnSubmit">
                    <a class="btn btn-warning btn-lg" 
                       href="index.php" id="btnToTop">
                        <i class="fa fa-arrow-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $('#dt_nasc').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-br'
    });
</script>
    