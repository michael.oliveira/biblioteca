<?php
require_once "ClienteModel.php";
require_once "Conexao.php";

class ClienteDao {
    private static $instance;
    private $con;
    
    public function __construct(){
        $this->con = Conexao::getCon();
    }
    
    public function getCon(){
        return $this->con;
    }
    
    public static function getInstance(){
        if (null == self::$instance){
            self::$instance = new ClienteDao();
        }
        return self::$instance;
    }
    
    public function getAll(){
        $sql = "SELECT * FROM cliente";
        $stm = $this->con->prepare($sql);
        $stm->setFetchMode(\PDO::FETCH_CLASS, 'ClienteModel');
        $stm->execute();
        
        
        return $stm->fetchAll();
    }
    
    public function insert(ClienteModel $clienteModel){
        //var_dump($clienteModel);die;
        $sql = "INSERT INTO pessoa(nome, rg, cpf, endereco, data_nascimento, "
                . "bairro, cidade, estado) "
            . "VALUES(:nome, :rg, :cpf, :endereco, :dataNascimento, "
                . ":bairro, :cidade, :estado)";
        $stm = $this->con->prepare($sql);
        $stm->execute(array(
            'nome'  => $clienteModel->getPessoa()->getNome(),
            'rg'    => $clienteModel->getPessoa()->getRg(),
            'cpf'    => $clienteModel->getPessoa()->getCpf(),
            'dataNascimento'    => $clienteModel->getPessoa()->getData_nascimento(),
            'endereco'    => $clienteModel->getPessoa()->getEndereco(),
            'bairro'    => $clienteModel->getPessoa()->getBairro(),
            'cidade'    => $clienteModel->getPessoa()->getCidade(),
            'estado'    => $clienteModel->getPessoa()->getEstado()
        ));
        $sql2 = "INSERT INTO cliente(cod_pessoa) values(:pessoa)";
        $stm2 = $this->con->prepare($sql2);
        $stm2->execute(array(
            'pessoa'    => $this->con->lastInsertId()
        ));
    }
    
    public function update($id, ClienteModel $clienteModel){
        $sql = "UPDATE cliente SET "
                . "nome = :nome, "
                . "rg = :rg, "
                . "cpf = :cpf, "
                . "data_nascimento = :dataNascimento, "
                . "endereco = :endereco, "
                . "bairro = :bairro, "
                . "cidade = :cidade, "
                . "uf = :uf "
                . "WHERE cod_cliente = :id";
        $stm = $this->getCon()->prepare($sql);
        $stm->execute(array(
            'nome'  => $clienteModel->getNome(),
            'rg'    => $clienteModel->getRg(),
            'cpf'    => $clienteModel->getCpf(),
            'dataNascimento'    => $clienteModel->getDataNascimento(),
            'endereco'    => $clienteModel->getEndereco(),
            'bairro'    => $clienteModel->getBairro(),
            'cidade'    => $clienteModel->getCidade(),
            'uf'    => $clienteModel->getUf(),
            'id'    => $id
        ));
    }
    
    public function getCliente($id){
        $sql = "SELECT * FROM cliente WHERE cod_cliente = :id";
        $stm = $this->getCon()->prepare($sql);
        $stm->bindValue('id', $id);
        $stm->execute();
        return $stm->fetchObject('ClienteModel');
    }
    
    public function insertUser($id, $data){
        $sql = "INSERT INTO usuario(nome, senha) VALUES(:nome, :senha)";
        $stm = $this->getCon()->prepare($sql);
        $stm->execute(array(
            'nome'  => $data['user_name'],
            'senha' => md5($data['senha'])
        ));
        $sql2 = "UPDATE cliente SET cod_usuario = :cod_user "
                . "WHERE cod_cliente = $id";
        $stm = $this->getCon()->prepare($sql2);
        $stm->execute(array(
            'cod_user'  => $this->getCon()->lastInsertId()
        ));
        
    }
    
    public function delete($id){
        $sql = 'DELETE FROM cliente WHERE cod_cliente = :id';
        $stm = $this->getCon()->prepare($sql);
        $stm->bindValue('id', $id);
        $stm->execute();
    }
    
    public function dataEua($data){
        $data = DateTime::createFromFormat("d/m/Y", $data)
                    ->format("Y-m-d");
        
        return $data;  
    }
    
    public function dataBr($data){
        $data = DateTime::createFromFormat("Y-m-d", $data)
                       ->format("d/m/Y");
        
        return $data; 
    }
    
    public function getEstados(){
        $estados = array(
            "AP"    => "AP",
            "PA"    => "PA",
            "Amazonas"    => "AM",
            "MA"    => "MA",
            "RN"    => "RN",
            "RJ"    => "RJ",
            "SP"    => "SP",
            "MG"    => "MG",
            "TO"    => "TO"
        );
        
        return $estados;
    }
}
