<?php 
    include "layout.php";
    require "ClienteDao.php";

    $clienteDao = ClienteDao::getInstance();
    $clientes = $clienteDao->getAll();
?>
<div class="container">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <div class="panel-title">
                Clientes
            </div>

        </div>

        <div class="panel-body"> 
            <div class="row">
                <div class="col-sm-3">
                    <a href="adicionar.php" class="btn btn-primary">
                        Novo
                    </a>
                </div>
                <div class="col-sm-8 form-inline">
                    <?php 
                        $loc_cliente = (isset($_GET['loc_cliente'])) ?
                                        $_GET['loc_cliente'] : "";
                    ?>
                    <form action="index.php?cliente=<?php echo $loc_cliente?>" 
                          id="frm_loc_cli"
                          method="get">
                        <div class="form-group">
                            <input name="loc_cliente" size="50" type="text" 
                                   class="form-control" 
                                   placeholder="Entre com iniciais do nome"
                                   id="loc_cli">
                        </div>
                        <button id="btn_loc" name="btn_loc" 
                                type="submit" class="btn btn-primary">Procurar</button>
                    </form>
                </div>
            </div>
            <table class="table table-bordered" style="margin-top: 10px">
                <tr>
                    <td>Código</td>
                    <td>Nome</td>    
                    <td>RG</td>    
                    <td>Ações</td>
                </tr>

                <?php
                    if (isset($_GET['loc_cliente'])){
                        $sql = "SELECT * FROM cliente WHERE nome like :nome";
                        $query = $con->prepare($sql);
                        $query->bindValue("nome", $_GET['loc_cliente'] . "%");
                        $query->execute();

                    }
                    
                    foreach($clientes as $cliente):    
                ?>

                <tr>
                    <td><?php echo $cliente->getCod_cliente(); ?></td>
                    <td><?php echo $cliente->getPessoa()->getNome(); ?></td>    
                    <td><?php echo $cliente->getPessoa()->getRg(); ?></td>    
                    <td>
            <a class="glyphicon glyphicon-edit"
                href="editar.php?id=<?php echo $cliente->getCod_cliente(); ?>"></a>
                        <a class="glyphicon glyphicon-user"
                href="user.php?id=<?php echo $cliente->getCod_cliente(); ?>"></a>
            <a href="#" 
            class="glyphicon glyphicon-trash text-danger"
            onclick="confirm(
                'Deletar Cliente', 
                'Tem certeza que deseja excluir o cliente \n\
                    <?php echo $cliente->getPessoa()->getNome() ?> ?',
                'Close',
                'Deletar',
                null,'recebe.php?op=delete&id=<?php echo $cliente->getCod_cliente() ?>')"
           ></a>
                    </td>
                </tr>
        <?php
            endforeach;      
        ?>

        </table>
    </div>                
</div>

</div>

<script>
    $('#deletar').click(function(){
        
        
    });
</script>

    </body>
</html>
