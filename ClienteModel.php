<?php
require_once "UserModel.php";
require_once "PessoaModel.php";
require_once "Conexao.php";

class ClienteModel {
    private $con;
    private $cod_cliente;
    private $cod_usuario;
    private $cod_pessoa;
    private $usuario;
    private $pessoa;
    
    public function __construct() {
        if (null == $this->con){
            $this->con = Conexao::getCon();
        }
    }
    
    public function getCod_cliente() {
        return $this->cod_cliente;
    }

    public function getCod_usuario() {
        return $this->cod_usuario;
    }

    public function getCod_pessoa() {
        return $this->cod_pessoa;
    }

    public function getUsuario() {
        if ($this->cod_usuario !== null){
            $sql = "SELECT * FROM usuario WHERE cod_usuario = :id";
            $stm = $this->con->prepare($sql);
            $stm->bindValue('id', $this->cod_usuario);
            $stm->execute();
            return $stm->fetchObject('UserModel');
        } else {
            return $this->usuario;
        }
    }

    public function getPessoa() {
        if ($this->cod_pessoa !== null){
            $sql = "SELECT * FROM pessoa WHERE cod_pessoa = :id";
            $stm = $this->con->prepare($sql);
            $stm->bindValue('id', $this->cod_pessoa);
            $stm->execute();
            return $stm->fetchObject('PessoaModel');
        } else {
            return $this->pessoa;
        }
    }

    public function setCod_cliente($cod_cliente) {
        $this->cod_cliente = $cod_cliente;
    }

    public function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }

    public function setCod_pessoa($cod_pessoa) {
        $this->cod_pessoa = $cod_pessoa;
    }

    public function setUsuario(UserModel $userModel) {
        $this->usuario = $userModel;
    }

    public function setPessoa(PessoaModel $pessoa) {
        $this->pessoa = $pessoa;
    }

    
}