<?php

class Conexao {
    private static $con;
    
    public static function getCon(){
        if (null == self::$con){
            self::$con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "Fms237691");
            self::$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$con;
    }
}
